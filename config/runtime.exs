import Config

config :mxmv, default_adapter: "mix"

config :mxmv, adapters: [
  %{
    name: "mix",
    module: Mxmv.Adapters.Mix
  },
  %{
    name: "manifest",
    module: Mxmv.Adapters.ChromeExtensionManifest
  }]
