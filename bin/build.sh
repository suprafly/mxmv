#!/usr/bin/env bash

export MIX_ENV=prod

mix release

cp _build/prod/rel/bakeware/mxmv ~/bin
echo "mxmv copied to ~/bin"
