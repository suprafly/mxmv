defmodule Mxmv.MixProject do
  use Mix.Project

  def project do
    [
      app: :mxmv,
      version: "0.1.0",
      elixir: "~> 1.11",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      releases: releases(),
      preferred_cli_env: [release: :prod]
    ]
  end

  def application do
    [
      extra_applications: [:logger],
      mod: {Mxmv, []}
    ]
  end

  defp deps do
    [
      {:bakeware, git: "https://github.com/bake-bake-bake/bakeware"},
      {:jason, "~> 1.2"}
    ]
  end

  defp releases do
    [
      mxmv: [
        steps: [:assemble, &Bakeware.assemble/1],
        overwrite: true,
        strip_beams: Mix.env() == :prod
      ]
    ]
  end
end
