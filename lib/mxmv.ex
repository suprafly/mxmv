defmodule Mxmv do
  @moduledoc """
  A tool for working with mix.exs project version numbers.

  Options:
  `-m`, or `--major` - updates the major number
  `-n`, or `--minor` - updates the minor number
  `-p`, or `--patch` - updates the patch number
  `--pre` - updates the pre-release version number
  `--build` - updates the build version number
  `-c`, or `--commit` - generates a commit if run with the `up` command
  """
  use Bakeware.Script
  alias Mxmv.{Utils, Version}

  def default_adapter() do
    Application.get_env(:mxmv, :default_adapter)
  end

  def adapters() do
    Application.get_env(:mxmv, :adapters)
  end

  @impl Bakeware.Script
  def main(["which" | _]) do
    adapters = adapters()
    |> Enum.map(& &1.name)
    |> Enum.join(", ")

    IO.puts("mxmv available adapters are: #{adapters}")
  end

  def main([name | _] = args) do
    with adapters <- adapters(),
         true <- adapters |> Enum.map(& &1.name) |> Enum.member?(name),
         adapter <- Enum.find(adapters, & &1.name == name),
         {path, argv} <- get_path_and_argv(args),
         {action, opts, invalid_opts} <- Utils.get_action_and_opts(argv),
         mxm_version <- %Version{
          path: path,
          action: action,
          opts: opts,
          invalid_opts: invalid_opts,
          commit: opts[:commit],
          module: adapter.module
        }
    do
      mxm_version
      |> try_invalid_opts()
      |> do_main()
    else
      false ->
        IO.puts("mxmv: invalid name '#{name}'")
    end
    :ok
  end

  defp get_path_and_argv(["mix" | argv]) do
    {"mix.exs", argv}
  end

  defp get_path_and_argv([_name, path | argv]) do
    {path, argv}
  end

  defp try_invalid_opts(%Version{invalid_opts: invalid_opts} = mxm_version) when invalid_opts != [] do
    Utils.invalid_opts(invalid_opts)
    %{mxm_version | halt: true}
  end

  defp try_invalid_opts(mxm_version) do
    mxm_version
  end

  defp do_main(%Version{halt: true}) do
    :ok
  end

  defp do_main(%Version{action: :up} = mxm_version) do
    mxm_version
    |> run(:version)
    |> run(:increase_version)
    |> run(:overwrite)
    |> run(:commit)
  end

  defp do_main(%Version{action: :get} = mxm_version) do
    mxm_version
    |> run(:version)
    |> run(:increase_version)
    |> print_version_string()
  end

  defp do_main(%Version{} = mxm_version) do
    mxm_version
    |> run(:version)
    |> print_version_string()
  end

  defp print_version_string(%Version{version_string: version_string}) do
    IO.puts(version_string)
  end

  def run(%Version{module: module} = mxm_version, function) do
    apply(module, function, [mxm_version])
  end
end
