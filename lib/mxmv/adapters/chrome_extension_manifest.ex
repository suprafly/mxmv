defmodule Mxmv.Adapters.ChromeExtensionManifest do
  @moduledoc """
  A tool for working with Chrome Extension manifest.json project version numbers.

  This is a subset of the full features, as it is built specifically for my own needs.

  In particular, the versioning is not semantic because I use a fourth field for
  revision number.
  """
  alias Mxmv.Utils

  @behaviour Mxmv.Adapter

  @impl Mxmv.Adapter
  def version(%Mxmv.Version{path: path} = mxm_version) do
    with true <- File.exists?(path),
         data <- path |> File.read!() |> Jason.decode!(),
         version_string <- data["version"],
         [m, n, p | [rev]] <- String.split(version_string, "."),
         version_parsed <- [m, n, p] |> Enum.join(".") |> Version.parse!(),
         revision <- String.to_integer(rev)
    do
      %{mxm_version |
        original_version: version_string,
        version_string: version_string,
        version_parsed: version_parsed,
        assigns: [revision: revision]
        }
    end
  end

  @impl Mxmv.Adapter
  def increase_version(%Mxmv.Version{version_parsed: version_parsed, assigns: [revision: revision], opts: opts} = mxm_version) do
    with version <- version_parsed |> Map.from_struct() |> Map.put(:revision, revision),
         new_version <- Utils.increase_version!(version, opts),
         new_version_string <- deconstruct_version!(new_version, opts)
    do
      %{mxm_version |
        version_string: new_version_string,
        version_parsed: new_version
        }
    end
  end

  def increase_version(%Mxmv.Version{version_parsed: version_parsed, opts: opts} = mxm_version) do
    version_parsed = Utils.increase_version!(version_parsed, opts)
    %{mxm_version |
      version_string: to_string(version_parsed),
      version_parsed: version_parsed
      }
  end

  defp deconstruct_version!(version, [patch: true]) do
    "#{version.major}.#{version.minor}.#{version.patch}"
    |> Version.parse!()
  end

  defp deconstruct_version!(version, _parsed_opts) do
    "#{version.major}.#{version.minor}.#{version.patch}.#{version.revision}"
  end

  @impl Mxmv.Adapter
  def overwrite(%Mxmv.Version{path: path, version_string: version_string} = mxm_version) do
    path
    |> File.read!()
    |> Jason.decode!()
    |> Map.put("version", version_string)
    |> Jason.encode!(pretty: true)
    |> Utils.write!(path)

    mxm_version
  end

  @impl Mxmv.Adapter
  def commit(%Mxmv.Version{path: path, version_string: version_string, commit: commit} = mxm_version) do
    version_string
    |> commit_msg(path)
    |> Utils.maybe_commit(path, commit)

    mxm_version
  end

  def commit_msg(version, path) do
    "Updated #{path} version: #{version}"
  end
end
