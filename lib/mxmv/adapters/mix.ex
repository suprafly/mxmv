defmodule Mxmv.Adapters.Mix do
  @moduledoc """
  A tool for working with mix.exs project version numbers.

  Options:
  `-m`, or `--major` - updates the major number
  `-n`, or `--minor` - updates the minor number
  `-p`, or `--patch` - updates the patch number
  `--pre` - updates the pre-release version number
  `--build` - updates the build version number
  `-c`, or `--commit` - generates a commit if run with the `up` command
  """
  alias Mxmv.Utils

  @behaviour Mxmv.Adapter

  @impl Mxmv.Adapter
  def version(mxm_version) do
    with regex <- ~r{version\:\s+\"([0-9\.]+)\"},
         mix_data <- File.read!("mix.exs"),
         results <- Regex.run(regex, mix_data),
         version_string <- List.last(results)
    do
      %{mxm_version |
        original_version: version_string,
        version_string: version_string,
        version_parsed: Version.parse!(version_string)
        }
    end
  end

  @impl Mxmv.Adapter
  def increase_version(%Mxmv.Version{version_parsed: version_parsed, opts: opts} = mxm_version) do
    version_parsed = Utils.increase_version!(version_parsed, opts)
    %{mxm_version |
      version_string: to_string(version_parsed),
      version_parsed: version_parsed
      }
  end

  @impl Mxmv.Adapter
  def overwrite(%Mxmv.Version{path: path, original_version: original_version, version_string: version_string} = mxm_version) do
    path
    |> File.read!()
    |> String.replace("version: \"#{original_version}\",", "version: \"#{version_string}\",")
    |> Utils.write!(path)

    mxm_version
  end

  @impl Mxmv.Adapter
  def commit(%Mxmv.Version{path: path, version_string: version_string, commit: commit} = mxm_version) do
    version_string
    |> commit_msg()
    |> Utils.maybe_commit(path, commit)

    mxm_version
  end

  defp commit_msg(version) do
    "Updated mix.exs version: #{version}"
  end
end
