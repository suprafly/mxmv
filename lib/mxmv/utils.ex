defmodule Mxmv.Utils do
  @moduledoc """
  Helper functions for versioning.
  """
  @opts [
    aliases: [
      m: :major,
      n: :minor,
      p: :patch,
      r: :revision,
      c: :commit,
    ],
    strict: [
      major: :boolean,
      minor: :boolean,
      patch: :boolean,
      revision: :boolean,
      pre:   :string,
      build: :string,
      commit: :boolean
    ]
  ]
  @sort_order Enum.map(@opts[:strict], fn {k, _} -> k end)

  defp opts do
    @opts
  end

  defp sort_opts(opts) do
    find_index = fn x -> Enum.find_index(@sort_order, fn _ -> x end) end
    Enum.sort(opts, fn {k1, _}, {k2, _} -> find_index.(k1) < find_index.(k2) end)
  end

  def get_action_and_opts(argv) do
    with {opts, [action], invalid_opts} <- OptionParser.parse(argv, opts()),
         action <- String.to_atom(action)
    do
      {action, opts, invalid_opts}
    end
  end

  def invalid_opts(opts) do
    opts
    |> Enum.map(fn {n, _} -> "mxmv: invalid option '#{n}'" end)
    |> List.insert_at(-1, "Try 'mxmv --help' for more information.")
    |> Enum.join("\n")
    |> IO.puts()
  end

  def increase_version!(version, opts) do
    opts
    |> sort_opts()
    |> Enum.reduce(version, fn
      {:commit, _}, v ->
        v
      {:major, true}, v ->
        Map.merge(v, %{major: Map.get(v, :major, 0) + 1, minor: 0, patch: 0})
      {:minor, true}, v ->
        Map.merge(v, %{minor: Map.get(v, :minor, 0) + 1, patch: 0})
      {part, true}, v ->
        Map.put(v, part, Map.get(v, part, 0) + 1)
      {:pre, value}, v ->
        Map.put(v, :pre, String.split(value, "."))
      {part, value}, v ->
        Map.put(v, part, value)
    end)
  end

  def write!(buf, file) do
    File.write!(file, buf)
  end

  def maybe_commit(commit_msg, path, true) do
    System.cmd("git", ["add", path])
    System.cmd("git", ["commit", "-m", commit_msg])
    {hash, 0} = System.cmd("git", ["rev-parse", "HEAD"])
    hash = String.trim_trailing(hash)

    branch = System.cmd("git", ["rev-parse", "--abbrev-ref", "HEAD"])
    |> elem(0)
    |> String.trim_trailing()

    """
    `mxmv` created a new git commit:
    [#{branch} #{hash}] #{commit_msg}
     1 file changed, 1 insertions(+)
    """
    |> IO.puts()
  end

  def maybe_commit(commit_msg, _) do
    IO.puts(commit_msg)
  end
end
