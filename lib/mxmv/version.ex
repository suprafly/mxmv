defmodule Mxmv.Version do
  @moduledoc """
  A struct to hold Mxmv version info.
  """

  defstruct path: nil,
            action: nil,
            original_version: nil,
            version_string: nil,
            version_parsed: nil,
            opts: [],
            invalid_opts: [],
            commit: false,
            module: nil,
            halt: false,
            assigns: []

  def new(data) do
    struct(__MODULE__, data)
  end
end
