defprotocol Mxmv.Adapter do
  @moduledoc """
  A behaviour for defining new version adapters.
  """
  @callback version(Mxmv.Version.t()) :: Mxmv.Version.t()
  @callback increase_version(Mxmv.Version.t()) :: Mxmv.Version.t()
  @callback overwrite(Mxmv.Version.t()) :: Mxmv.Version.t()
  @callback commit(Mxmv.Version.t()) :: Mxmv.Version.t()
end
